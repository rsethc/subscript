build:
	mkdir -p bin
	gcc src/main.c -o bin/subscript

install: build
	cp bin/subscript /usr/bin/subscript
