#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
typedef char bool;
#define true 1
#define false 0
struct fbuf
{
	size_t len;
	size_t pos;
};
typedef struct fbuf fbuf;
char* fbuf_dataptr (fbuf* buf)
{
	return (char*)buf + sizeof(fbuf);
};
fbuf* fbuf_open (char* path)
{
	FILE* file = fopen(path,"r");
	if (!file) return NULL;
	size_t len = 0;
	while (fgetc(file) != EOF) len++;
	fseek(file,0,SEEK_SET);
	fbuf* out = malloc(sizeof(fbuf) + len);
	out->len = len;
	out->pos = 0;
	char* dataptr = fbuf_dataptr(out);
	while (len--) *dataptr++ = fgetc(file);
	fclose(file);
	return out;
};
bool fbuf_eof (fbuf* buf)
{
	return buf->pos >= buf->len;
};
int fbuf_getchar (fbuf* buf)
{
	int out;
	if (fbuf_eof(buf)) out = EOF;
	else out = fbuf_dataptr(buf)[buf->pos];
	buf->pos++;
	return out;
};
void fbuf_ungetc (fbuf* buf)
{
	if (buf->pos) buf->pos--;
};
size_t fbuf_getpos (fbuf* buf)
{
	return buf->pos;
};
void fbuf_setpos (fbuf* buf, size_t pos)
{
	buf->pos = pos;
};
void fbuf_close (fbuf* buf)
{
	free(buf);
};

void SkipWhitespace (fbuf* file)
{
	char cc;
	while (isspace(cc = fbuf_getchar(file)));
	fbuf_ungetc(file);
};
bool EndsString (char cc, bool quote, char esc_char)
{
	if (cc == EOF) return true;
	else if (quote) return cc == '"' && !esc_char;
	else return !!isspace(cc);
};
char* NextString (fbuf* file)
{
	SkipWhitespace(file);
	size_t len = 0;
	size_t size = 32;
	char* out = malloc(size);
	char cc = fbuf_getchar(file);
	bool quote = cc == '"';
	if (!quote) fbuf_ungetc(file);
	char esc_char = 0;
	while (1)
	{
		int cc = fbuf_getchar(file);
		if (quote && cc == '\\' && !esc_char)
		{
			esc_char = 1;
			continue;
		};
		if (EndsString(cc,quote,esc_char))
		{
			if (!len)
			{
				free(out);
				return NULL;
			};
			out[len] = 0;
			return out;
		};
		out[len++] = cc;
		if (len >= size)
		{
			out = realloc(out,size <<= 1);
		};
		esc_char = 0;
	};
};
bool StrEq (char* a, char* b)
{
	if (!a)
	{
		if (!b) return true;
		return !*b;
	}
	else if (!b) return !*a;
	while (true)
	{
		char aa = *a++;
		char bb = *b++;
		if (aa != bb) return false;
		if (!aa) return true;
	};
};
char* ReadTextFile (FILE* file)
{
	size_t len = 0;
	size_t size = 32;
	char* out = malloc(size);
	while (true)
	{
		char cc = fgetc(file);
		if (cc == EOF)
		{
			if (len)
			{
				out[len] = 0;
				return out;
			}
			else
			{
				free(out);
				return NULL;
			};
		};
		out[len++] = cc;
		if (len >= size)
		{
			out = realloc(out,size <<= 1);
		};
	};
};
size_t StrLen (char* str)
{
	if (!str) return 0;
	return strlen(str);
};
char* StrClone (char* str)
{
	size_t size = StrLen(str);
	if (!size) return NULL;
	char* out = malloc(++size);
	memcpy(out,str,size);
	return out;
};
char* StrReplace (char* body, char* replace, char* replacement)
{
	size_t body_len = StrLen(body);
	size_t replace_len = StrLen(replace);
	size_t replacement_len = StrLen(replacement);
	if (!body_len) return 0;
	if (!replace_len) return StrClone(body);
	size_t out_len = body_len;

	for (size_t scan_pos = 0; scan_pos + replace_len <= body_len; )
	{
		for (size_t scan_subpos = 0; scan_subpos < replace_len; scan_subpos++)
		{
			if (body[scan_pos + scan_subpos] != replace[scan_subpos]) goto KEEPTRYING;
		};
		goto MATCHED;

		KEEPTRYING:
		scan_pos++;
		continue;

		MATCHED:
		out_len += replacement_len;
		out_len -= replace_len;
		scan_pos += replace_len;
	};

	char* out = malloc(out_len + 1);
	out[out_len] = 0;
	size_t out_pos = 0;
	size_t scan_pos;
	for (scan_pos = 0; scan_pos + replace_len <= body_len; )
	{
		for (size_t scan_subpos = 0; scan_subpos < replace_len; scan_subpos++)
		{
			if (body[scan_pos + scan_subpos] != replace[scan_subpos]) goto KEEPTRYING2;
		};
		goto MATCHED2;

		KEEPTRYING2:
		out[out_pos++] = body[scan_pos];
		scan_pos++;
		continue;

		MATCHED2:
		memcpy(out + out_pos,replacement,replacement_len);
		out_pos += replacement_len;
		scan_pos += replace_len;
	};
	while (scan_pos < body_len) out[out_pos++] = body[scan_pos++];
	return out;
};
struct Var
{
	char* name;
	char* value;
};
typedef struct Var Var;
size_t VarCapacity = 16;
Var* Vars;
size_t VarCount = 0;
void SetVar (char* name, char* value)
{
	if (!name) goto DISCARDED;
	if (!*name)
	{
		free(name);
		goto DISCARDED;
	};

	for (size_t pos = 0; pos < VarCount; pos++)
	{
		Var* var = Vars + pos;
		if (StrEq(var->name,name))
		{
			if (var->value) free(var->value);
			free(name);
			var->value = value;
			return;
		};
	};

	if (VarCount >= VarCapacity)
	{
		Vars = realloc(Vars,sizeof(Var) * (VarCapacity <<= 1));
	};

	Var* var = Vars + VarCount++;
	var->name = name;
	var->value = value;

	return;

	DISCARDED:
	if (value) free(value);
};
char* GetVar (char* name)
{
	if (!name) return NULL;
	if (!*name) return NULL;
	for (size_t pos = 0; pos < VarCount; pos++)
	{
		Var* var = Vars + pos;
		if (StrEq(var->name,name)) return StrClone(var->value);
	};
	return NULL;
};
char* Combine (char* a, char* b)
{
	size_t len_a = StrLen(a);
	size_t len_b = StrLen(b);
	if (!len_a) return StrClone(b);
	if (!len_b) return StrClone(a);
	size_t out_len = len_a + ++len_b; // b longer to include null char
	char* out = malloc(out_len);
	memcpy(out,a,len_a);
	memcpy(out + len_a,b,len_b);
	return out;
};
struct Function
{
	char* name;
	char* (*procedure) (fbuf* file);
};
typedef struct Function Function;
Function* Functions;
size_t FunctionCapacity = 16;
size_t FunctionCount = 0;
void RegisterFunction (char* name, char* (*procedure) (fbuf* file))
{
	if (FunctionCount >= FunctionCapacity)
		Functions = realloc(Functions,sizeof(Function) * (FunctionCapacity <<= 1));
	Functions[FunctionCount++] = (Function){name,procedure};
};
Function* LookupFunction (char* name)
{
	for (size_t pos = 0; pos < FunctionCount; pos++)
	{
		Function* func = Functions + pos;
		if (StrEq(func->name,name)) return func;
	};
	return NULL;
};
volatile size_t DoNotExecute = 0;
char* Interpret (fbuf* file)
{
	char* cmd = NextString(file);
	if (!cmd) return NULL;
	Function* func = LookupFunction(cmd);
	if (func)
	{
		free(cmd);
		char* result = func->procedure(file);
		if (DoNotExecute)
		{
			if (result) free(result);
			return NULL;
		}
		else return result;
	}
	else
	{
		if (DoNotExecute)
		{
			free(cmd);
			return NULL;
		}
		else return cmd;
	};
};
char* Environment = NULL;
FILE* fopen_local (char* path, char* mode)
{
	char* longpath = Combine(Environment,path);
	FILE* file = fopen(longpath,mode);
	free(longpath);
	return file;
};
fbuf* fbuf_open_local (char* path)
{
	char* longpath = Combine(Environment,path);
	fbuf* file = fbuf_open(longpath);
	free(longpath);
	return file;
};
char* func_path (fbuf* file)
{
	char* localpath = Interpret(file);

	if (DoNotExecute) return NULL;

	char* longpath = Combine(Environment,localpath);
	if (localpath) free(localpath);
	return longpath;
};
char* func_localpath (fbuf* file)
{
	char* longpath = Interpret(file);

	if (DoNotExecute) return NULL;

	size_t envlen = StrLen(Environment);
	size_t longlen = StrLen(longpath);
	if (longlen < envlen)
	{
		printf(" > Invalid long path \"%s\" for environment \"%s\", cannot localize.\n",longpath,Environment);
		return NULL; // bad input
	};
	char* local = StrClone(longpath + envlen);
	free(longpath);
	return local;
};
char* func_var (fbuf* file)
{
	char* varname = Interpret(file);

	if (DoNotExecute) return NULL;

	char* varvalue = GetVar(varname);
	if (varname) free(varname);
	return varvalue;
};
char* func_setvar (fbuf* file)
{
	char* varname = Interpret(file);
	char* varvalue = Interpret(file);

	if (DoNotExecute) return NULL;

	SetVar(varname,varvalue);
	return StrClone(varvalue);
};
char* func_combine (fbuf* file)
{
	char* a = Interpret(file);
	char* b = Interpret(file);

	if (DoNotExecute) return NULL;

	char* out = Combine(a,b);
	if (a) free(a);
	if (b) free(b);
	return out;
};
char* func_write (fbuf* file)
{
	char* dest_path = Interpret(file);
	char* write_data = Interpret(file);

	if (DoNotExecute) return NULL;

	if (dest_path)
	{
		FILE* dest_file = fopen_local(dest_path,"w");
		if (dest_file)
		{
			if (write_data)
			{
				char* write_ptr = write_data;
				while (*write_ptr)
				{
					fputc(*write_ptr++,dest_file);
				}
			}
			else printf("! > No write data for \"%s\".\n",dest_path);
			fclose(dest_file);
		}
		else printf("! > Can't open \"%s\" for writing.\n",dest_path);
		free(dest_path);
	}
	else printf("! > No file path for write command.\n");
	return write_data;
}
char* func_load (fbuf* file)
{
	char* file_path = Interpret(file);

	if (DoNotExecute) return NULL;

	char* out_buf = NULL;
	if (file_path)
	{
		FILE* read_file = fopen_local(file_path,"r");
		if (read_file)
		{
			out_buf = ReadTextFile(read_file);
		}
		else printf("! > Can't open \"%s\" for reading.\n",file_path);
		free(file_path);
	}
	else printf("! > No file path for read command.\n");
	return out_buf;
};
char* func_replace (fbuf* file)
{
	char* body = Interpret(file);
	char* replace = Interpret(file);
	char* replacement = Interpret(file);

	if (DoNotExecute) return NULL;

	char* product = StrReplace(body,replace,replacement);
	if (body) free(body);
	if (replace) free(replace);
	if (replacement) free(replacement);
	return product;
};
char* func_display (fbuf* file)
{
	char* to_say = Interpret(file);

	if (DoNotExecute) return NULL;

	printf("  > %s\n",to_say);
	return to_say;
};
char* func_wait (fbuf* file)
{
	if (DoNotExecute) return NULL;

	getchar();
	return NULL;
};
char* func_slashstar (fbuf* file)
{
	bool ended = false;
	while (!fbuf_eof(file) && !ended)
	{
		char* end = NextString(file);
		ended = StrEq(end,"*/");
		free(end);
	};
	if (!ended)
	{
		// We didn't encounter an end to this comment.
		printf("! > Comment start (/*) without corresponding comment end (*/).\n");
	};
	return Interpret(file);
};
char* func_starslash (fbuf* file)
{
	// This shouldn't be encountered on its own!
	printf("! > Comment end (*/) without corresponding comment start (/*).\n");
	return NULL;
};
char* func_slashslash (fbuf* file)
{
	// Line comment.
	fbuf_ungetc(file); // Was '//' itself broken by newline? Go back to check.
	while (fbuf_getchar(file) != '\n' && !fbuf_eof(file));
	return Interpret(file);
};
char* StripFilename (char* fullpath)
{
	size_t last_slash = 0;
	size_t pathlen = StrLen(fullpath);
	for (size_t pos = 0; pos < pathlen; pos++)
	{
		char cc = fullpath[pos];
		if (
			cc == '/'
			#ifdef _WIN32
			|| cc == '\\'
			#endif
		) last_slash = pos;
	};
	if (!last_slash) return NULL;
	char* out = malloc(++last_slash + 1);
	memcpy(out,fullpath,last_slash);
	out[last_slash] = 0;
	return out;
};
void ExecuteFile (fbuf* file, char* LowerEnvironment);
void MarkRoutinesGarbageable (fbuf* pgmfile);
char* func_exec (fbuf* file)
{
	char* pgmpath = Interpret(file);

	if (DoNotExecute) return NULL;

	if (pgmpath)
	{
		char* dir = StripFilename(pgmpath);
		fbuf* pgmfile = fbuf_open_local(pgmpath);
		if (pgmfile)
		{
			ExecuteFile(pgmfile,dir);
			MarkRoutinesGarbageable(pgmfile);
		}
		else printf("! > Cannot open \"%s\" for reading.\n",pgmpath);
		free(pgmpath);
	}
	else printf("! > No path given to exec command.\n");
	return NULL;
};
char* func_cmd (fbuf* file)
{
	char* pgmpath = Interpret(file);

	if (DoNotExecute) return NULL;

	if (pgmpath)
	{
		system(pgmpath);
		free(pgmpath);
	}
	else printf("! > No command string given to cmd command.\n");
	return NULL;
};
struct Routine
{
	char* name;
	fbuf* file;
	size_t filepos;
	bool garbageable;
};
typedef struct Routine Routine;
Routine* Routines;
size_t RoutineCount = 0;
size_t RoutineCapacity = 16;
bool IsOnlyGarbage (Routine* other_than)
{
	Routine* routine = Routines;
	for (size_t i = 0; i < RoutineCount; i++, routine++) if (routine != other_than && routine->name && routine->file == other_than->file) return false;
	return true;
};
void TryGarbage (Routine* routine)
{
	if (routine->garbageable) if (IsOnlyGarbage(routine))
	{
		fbuf_close(routine->file);
	};
};
void RegisterRoutine (char* name, fbuf* file, size_t filepos)
{
	size_t index;
	for (index = 0; index < RoutineCount; index++)
		if (Routines[index].name)
			if (StrEq(Routines[index].name,name))
			{
				if (Routines[index].file != file)
					// ^ "Not necessary because in order to define something late, it would have to be done through an existing functions."
					//		 ^ Not always true because the function could redefine itself inside itself.
					TryGarbage(Routines + index);
				free(Routines[index].name);
				goto SET;
			};
	for (index = 0; index < RoutineCount; index++)
		if (!Routines[index].name)
			goto SET;
	if (++RoutineCount > RoutineCapacity) Routines = realloc(Routines,sizeof(Routine) * (RoutineCapacity <<= 1));
	SET:
	Routines[index] = (Routine){name,file,filepos,false};
};
void DeregisterRoutinesByFile (fbuf* file)
{
	Routine* routine = Routines;
	for (size_t pos = 0; pos < RoutineCount; pos++, routine++)
	{
		if (routine->name && routine->file == file)
		{
			free(routine->name);
			routine->name = NULL;
		};
	};
	// No routines are ever deregistered until exit:
};
void MarkRoutinesGarbageable (fbuf* file)
{
	bool any = false;
	for (size_t i = 0; i < RoutineCount; i++)
		if (Routines[i].name && Routines[i].file == file)
		{
			Routines[i].garbageable = true;
			any = true;
		};
	if (!any) fbuf_close(file);
};
volatile size_t OpenCalls = 0;
volatile bool EndRoutine = false;
void RunRoutine (Routine* routine)
{
	fbuf_setpos(routine->file,routine->filepos);
	OpenCalls++;
	while (!EndRoutine && !fbuf_eof(routine->file))
	{
		char* result = Interpret(routine->file);
		if (result) free(result);
	};
	EndRoutine = false;
	OpenCalls--;
};
void CallRoutine (fbuf* file, char* routine_name)
{
	Routine* routine;
	for (size_t i = 0; i < RoutineCount; i++)
	{
		routine = Routines + i;
		if (routine->name) if (StrEq(routine->name,routine_name)) goto FOUND;
	};
	printf("! > Attempted to call routine \"%s\" but it does not exist.\n",routine_name);
	return;
	FOUND:

	if (routine->file == file)
	{
		size_t return_to = fbuf_getpos(file);
		RunRoutine(routine);
		fbuf_setpos(file,return_to);
	}
	else RunRoutine(routine);
};
char* func_routine (fbuf* file)
{
	char* routine_name = Interpret(file);
	if (!DoNotExecute)
	{
		if (!routine_name) printf("! > Attempt to declare routine with empty name.\n");
		else
		{
			size_t pos = fbuf_getpos(file);
			RegisterRoutine(routine_name,file,pos);
		};
	};

	DoNotExecute++;
	while (!EndRoutine)
	{
		if (fbuf_eof(file))
		{
			printf("! > Routine definition with no corresponding \"end\".\n");
			break;
		};
		Interpret(file); // Always returns null because DoNotExecute is on.
	};
	EndRoutine = false;
	DoNotExecute--;
	return NULL;
};
char* func_end (fbuf* file)
{
	if (!DoNotExecute && !OpenCalls) printf("! > Stray \"end\" without a corresponding routine definition.\n");
	else EndRoutine = true;
	return NULL;
};
char* func_do (fbuf* file)
{
	char* routine_name = Interpret(file);
	if (DoNotExecute) return NULL;
	if (routine_name) CallRoutine(file,routine_name);
	else printf("! > Routine name empty.\n");
	return NULL;
};
char* func_for_each_enclosed (fbuf* file)
{
	char* input = Interpret(file);
	char* start = Interpret(file);
	char* end = Interpret(file);
	char* routine_name = Interpret(file);
	char* varname = Interpret(file);

	if (DoNotExecute) return NULL;

	if (!start) printf("! > No start phrase provided to for-each-enclosed function.\n");
	if (!end) printf("! > No end phrase provided to for-each-enclosed function.\n");
	if (!varname) printf("! > No variable name provided to for-each-each-enclosed function.\n");
	if (!routine_name) printf("! > No routine name provided to for-each-enclosed function.\n");
	if (!start || !end || !input || !varname || !routine_name) return NULL;

	char* cloned = StrClone(input);
	size_t inputlen = StrLen(input);
	size_t startlen = StrLen(start);
	size_t endlen = StrLen(end);
	size_t startpos = 0;
	size_t KeepStart = 0;
	char* out = NULL;
	char* prev_out;
	for (size_t pos = 0; pos < inputlen; pos++)
	{
		char inputcc = input[pos];
		char startcc = start[startpos];
		if (inputcc == startcc)
		{
			if (++startpos >= startlen)
			{
				startpos = 0;
				size_t SectionStart = ++pos;
				prev_out = out;
				char prevcc = cloned[SectionStart];
				cloned[SectionStart] = 0;
				out = Combine(out,cloned + KeepStart);
				cloned[SectionStart] = prevcc;
				if (prev_out) free(prev_out);
				KeepStart = SectionStart;

				size_t endpos = 0;
				for ( ; pos < inputlen; pos++)
				{
					inputcc = input[pos];
					char endcc = end[endpos];
					if (inputcc == endcc)
					{
						if (++endpos >= endlen)
						{
							endpos = 0;
							size_t SectionEnd = pos-- - (endlen - 1);
							KeepStart = SectionEnd;
							prevcc = cloned[SectionEnd];
							cloned[SectionEnd] = 0;
							SetVar(StrClone(varname),StrClone(cloned + SectionStart));
							cloned[SectionEnd] = prevcc;
							CallRoutine(file,routine_name);
							prev_out = out;
							char* output = GetVar(varname);
							out = Combine(out,output);
							if (output) free(output);
							if (prev_out) free(prev_out);
							break;
						};
					}
					else endpos = 0;
				};
			};
		}
		else startpos = 0;
	};

	prev_out = out;
	out = Combine(out,cloned + KeepStart);
	if (prev_out) free(prev_out);

	free(input);
	free(start);
	free(end);
	free(varname); // Now incorrect: // Don't free varname because it stays with the var (since we did not clone it).
	free(routine_name);
	free(cloned);
	return out;
};
void InitMachine ()
{
	Vars = malloc(sizeof(Var) * VarCapacity);
	Functions = malloc(sizeof(Function) * FunctionCapacity);
	Routines = malloc(sizeof(Routine) * RoutineCapacity);



	RegisterFunction("var",func_var);
	RegisterFunction("setvar",func_setvar);

	RegisterFunction("routine",func_routine);
	RegisterFunction("end",func_end);
	RegisterFunction("do",func_do);

	RegisterFunction("display",func_display);
	RegisterFunction("load",func_load);
	RegisterFunction("write",func_write);

	RegisterFunction("combine",func_combine);
	RegisterFunction("replace",func_replace);
	RegisterFunction("for-each-enclosed",func_for_each_enclosed);

	RegisterFunction("wait",func_wait);

	RegisterFunction("exec",func_exec);

	RegisterFunction("cmd",func_cmd);

	RegisterFunction("path",func_path);
	RegisterFunction("localpath",func_localpath);



	// Comments
	RegisterFunction("/*",func_slashstar);
	RegisterFunction("*/",func_starslash);
	RegisterFunction("//",func_slashslash);
};
void QuitMachine ()
{
	for (size_t pos = 0; pos < VarCount; pos++)
	{
		Var* var = Vars + pos;
		free(var->name);
		if (var->value) free(var->value);
	};
	free(Vars);

	free(Functions);

	free(Routines);
};
void ExecuteFile (fbuf* file, char* LowerEnvironment)//, bool interactive)
{
	char* UpperEnvironment = Environment;
	Environment = Combine(Environment,LowerEnvironment);
	while (!fbuf_eof(file))
	{
		char* top_level = Interpret(file);
		if (top_level) free(top_level);
	};
	if (Environment) free(Environment);
	Environment = UpperEnvironment;
};
void CloseIncludes (fbuf* mainfile)
{
	for (size_t i = 0; i < RoutineCount; i++)
	{
		if (Routines[i].name)
		{
			fbuf* file = Routines[i].file;
			DeregisterRoutinesByFile(file);
			if (file != mainfile)
				fbuf_close(file);
		};
	};
};
int main (int argc, char** argv)
{
	fbuf* programfile;
	bool is_interactive = argc < 2;
	if (is_interactive)
	{
		printf("! > Program file path not provided.\n");
		getchar();
		return -1;
	};

	programfile = fbuf_open(argv[1]);
	if (!programfile)
	{
		printf("! > Could not open program file for reading.\n");
		getchar();
		return -2;
	};

	InitMachine();

	ExecuteFile(programfile,NULL);
	fbuf_close(programfile);
	CloseIncludes(programfile);
	QuitMachine();
	return 0;
};
